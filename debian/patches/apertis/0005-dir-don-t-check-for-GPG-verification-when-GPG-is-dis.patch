From 44e637a3dc377820820d8208f828ec33d7ca2b1d Mon Sep 17 00:00:00 2001
From: Arnaud Ferraris <arnaud.ferraris@collabora.com>
Date: Mon, 15 Feb 2021 18:23:14 +0100
Subject: [PATCH 05/26] dir: don't check for GPG verification when GPG is
 disabled

As we won't have any way to check GPG signatures are valid when its
support is disabled, this commit makes sure GPG-related checks aren't
enforced. This includes allowing collections even without GPG
signatures, and pulling from non-GPG verified remotes.

Similarly to what has been done for repos, it also prevents parsing
the GPG key in a ref file if GPG support is disabled.
---
 common/flatpak-dir.c | 39 +++++++++++++++++++++++++++++++++++++++
 1 file changed, 39 insertions(+)

--- a/common/flatpak-dir.c
+++ b/common/flatpak-dir.c
@@ -3769,8 +3769,10 @@
     {
       g_autofree char *remote_collection_id = NULL;
       const char *remote = remotes[i];
+#ifndef FLATPAK_DISABLE_GPG
       gboolean gpg_verify_summary;
       gboolean gpg_verify;
+#endif
 
       if (flatpak_dir_get_remote_disabled (self, remote))
         continue;
@@ -3779,6 +3781,7 @@
       if (remote_collection_id == NULL)
         continue;
 
+#ifndef FLATPAK_DISABLE_GPG
       if (!ostree_repo_remote_get_gpg_verify_summary (self->repo, remote, &gpg_verify_summary, NULL))
         continue;
 
@@ -3794,6 +3797,7 @@
           g_debug ("Migrating remote '%s' to gpg-verify-summary", remote);
           g_key_file_set_boolean (config, group, "gpg-verify-summary", TRUE);
         }
+#endif
     }
 
   if (config != NULL)
@@ -5145,8 +5149,10 @@
       g_autofree char *url = NULL;
       g_autoptr(GFile) child_repo_file = NULL;
       g_autofree char *child_repo_path = NULL;
+#ifndef FLATPAK_DISABLE_GPG
       gboolean gpg_verify_summary;
       gboolean gpg_verify;
+#endif
 
       if (!ostree_repo_remote_get_url (self->repo,
                                        state->remote_name,
@@ -5154,6 +5160,7 @@
                                        error))
         return FALSE;
 
+#ifndef FLATPAK_DISABLE_GPG
       if (!ostree_repo_remote_get_gpg_verify_summary (self->repo, state->remote_name,
                                                       &gpg_verify_summary, error))
         return FALSE;
@@ -5161,6 +5168,7 @@
       if (!ostree_repo_remote_get_gpg_verify (self->repo, state->remote_name,
                                               &gpg_verify, error))
         return FALSE;
+#endif
 
       if (is_oci)
         {
@@ -5172,6 +5180,7 @@
            * if necessary.
            */
         }
+#ifndef FLATPAK_DISABLE_GPG
       else if (!gpg_verify_summary || !gpg_verify)
         {
           /* The remote is not gpg verified, so we don't want to allow installation via
@@ -5183,6 +5192,7 @@
           if (!g_str_has_prefix (url, "file:"))
             return flatpak_fail_error (error, FLATPAK_ERROR_UNTRUSTED, _("Can't pull from untrusted non-gpg verified remote"));
         }
+#endif
       else
         {
           g_autoptr(OstreeRepo) child_repo = flatpak_dir_create_system_child_repo (self, &child_repo_lock, NULL, error);
@@ -6154,8 +6164,13 @@
                          g_variant_new_variant (g_variant_new_int32 (flags)));
   g_variant_builder_add (&builder, "{s@v}", "override-remote-name",
                          g_variant_new_variant (g_variant_new_string (remote_name)));
+#ifndef FLATPAK_DISABLE_GPG
   g_variant_builder_add (&builder, "{s@v}", "gpg-verify",
                          g_variant_new_variant (g_variant_new_boolean (TRUE)));
+#else
+  g_variant_builder_add (&builder, "{s@v}", "gpg-verify",
+                         g_variant_new_variant (g_variant_new_boolean (FALSE)));
+#endif
   g_variant_builder_add (&builder, "{s@v}", "gpg-verify-summary",
                          g_variant_new_variant (g_variant_new_boolean (FALSE)));
   g_variant_builder_add (&builder, "{s@v}", "inherit-transaction",
@@ -6196,8 +6211,10 @@
   g_autofree char *url = g_file_get_uri (path_file);
   g_autofree char *checksum = NULL;
   g_autofree char *current_checksum = NULL;
+#ifndef FLATPAK_DISABLE_GPG
   gboolean gpg_verify_summary;
   gboolean gpg_verify;
+#endif
   g_autoptr(OstreeGpgVerifyResult) gpg_result = NULL;
   g_autoptr(GVariant) old_commit = NULL;
   g_autoptr(OstreeRepo) src_repo = NULL;
@@ -6221,6 +6238,7 @@
   if (!flatpak_dir_repo_lock (self, &lock, LOCK_SH, cancellable, error))
     return FALSE;
 
+#ifndef FLATPAK_DISABLE_GPG
   if (!ostree_repo_remote_get_gpg_verify_summary (self->repo, remote_name,
                                                   &gpg_verify_summary, error))
     return FALSE;
@@ -6232,6 +6250,7 @@
   /* This was verified in the client, but lets do it here too */
   if (!gpg_verify_summary || !gpg_verify)
     return flatpak_fail_error (error, FLATPAK_ERROR_UNTRUSTED, _("Can't pull from untrusted non-gpg verified remote"));
+#endif
 
   if (!flatpak_repo_resolve_rev (self->repo, NULL, remote_name, ref, TRUE,
                                  &current_checksum, NULL, error))
@@ -6248,6 +6267,7 @@
   if (!flatpak_repo_resolve_rev (src_repo, NULL, remote_name, ref, FALSE, &checksum, NULL, error))
     return FALSE;
 
+#ifndef FLATPAK_DISABLE_GPG
   if (gpg_verify)
     {
       gpg_result = ostree_repo_verify_commit_for_remote (src_repo, checksum, remote_name, cancellable, error);
@@ -6257,6 +6277,7 @@
       if (ostree_gpg_verify_result_count_valid (gpg_result) == 0)
         return flatpak_fail_error (error, FLATPAK_ERROR_UNTRUSTED, _("GPG signatures found, but none are in trusted keyring"));
     }
+#endif
 
   g_clear_object (&gpg_result);
 
@@ -9723,8 +9744,10 @@
       g_autofree char *child_repo_path = NULL;
       FlatpakHelperDeployFlags helper_flags = 0;
       g_autofree char *url = NULL;
+#ifndef FLATPAK_DISABLE_GPG
       gboolean gpg_verify_summary;
       gboolean gpg_verify;
+#endif
       gboolean is_oci;
       gboolean is_revokefs_pull = FALSE;
 
@@ -9739,6 +9762,7 @@
                                        error))
         return FALSE;
 
+#ifndef FLATPAK_DISABLE_GPG
       if (!ostree_repo_remote_get_gpg_verify_summary (self->repo, state->remote_name,
                                                       &gpg_verify_summary, error))
         return FALSE;
@@ -9746,6 +9770,7 @@
       if (!ostree_repo_remote_get_gpg_verify (self->repo, state->remote_name,
                                               &gpg_verify, error))
         return FALSE;
+#endif
 
       is_oci = flatpak_dir_get_remote_oci (self, state->remote_name);
       if (no_pull)
@@ -9768,6 +9793,7 @@
           if (!flatpak_dir_mirror_oci (self, registry, state, flatpak_decomposed_get_ref (ref), opt_commit, NULL, token, progress, cancellable, error))
             return FALSE;
         }
+#ifndef FLATPAK_DISABLE_GPG
       else if (!gpg_verify_summary || !gpg_verify)
         {
           /* The remote is not gpg verified, so we don't want to allow installation via
@@ -9781,6 +9807,7 @@
           else
             return flatpak_fail_error (error, FLATPAK_ERROR_UNTRUSTED, _("Can't pull from untrusted non-gpg verified remote"));
         }
+#endif
       else
         {
           /* For system pulls, the pull has to be made in a child repo first,
@@ -10443,8 +10470,10 @@
       g_auto(GLnxLockFile) child_repo_lock = { 0, };
       g_autofree char *child_repo_path = NULL;
       FlatpakHelperDeployFlags helper_flags = 0;
+#ifndef FLATPAK_DISABLE_GPG
       gboolean gpg_verify_summary;
       gboolean gpg_verify;
+#endif
       gboolean is_revokefs_pull = FALSE;
 
       if (allow_downgrade)
@@ -10453,6 +10482,7 @@
 
       helper_flags = FLATPAK_HELPER_DEPLOY_FLAGS_UPDATE;
 
+#ifndef FLATPAK_DISABLE_GPG
       if (!ostree_repo_remote_get_gpg_verify_summary (self->repo, state->remote_name,
                                                       &gpg_verify_summary, error))
         return FALSE;
@@ -10460,6 +10490,7 @@
       if (!ostree_repo_remote_get_gpg_verify (self->repo, state->remote_name,
                                               &gpg_verify, error))
         return FALSE;
+#endif
 
       if (no_pull)
         {
@@ -10482,6 +10513,7 @@
                                        commit, NULL, token, progress, cancellable, error))
             return FALSE;
         }
+#ifndef FLATPAK_DISABLE_GPG
       else if (!gpg_verify_summary || !gpg_verify)
         {
           /* The remote is not gpg verified, so we don't want to allow installation via
@@ -10498,6 +10530,7 @@
           else
             return flatpak_fail_error (error, FLATPAK_ERROR_UNTRUSTED, _("Can't pull from untrusted non-gpg verified remote"));
         }
+#endif
       else
         {
           /* First try to update using revokefs-fuse codepath. If it fails, try to update using a
@@ -14417,8 +14450,10 @@
   is_runtime = g_key_file_get_boolean (keyfile, FLATPAK_REF_GROUP,
                                        FLATPAK_REF_IS_RUNTIME_KEY, NULL);
 
+#ifndef FLATPAK_DISABLE_GPG
   str = g_key_file_get_string (keyfile, FLATPAK_REF_GROUP,
                                FLATPAK_REF_GPGKEY_KEY, NULL);
+#endif
   if (str != NULL)
     {
       g_autofree guchar *decoded = NULL;
@@ -14454,8 +14489,10 @@
                                                             FLATPAK_REF_COLLECTION_ID_KEY);
     }
 
+#ifndef FLATPAK_DISABLE_GPG
   if (collection_id != NULL && gpg_data == NULL)
     return flatpak_fail_error (error, FLATPAK_ERROR_INVALID_DATA, _("Collection ID requires GPG key to be provided"));
+#endif
 
   *name_out = g_steal_pointer (&name);
   *branch_out = g_steal_pointer (&branch);
@@ -15245,6 +15282,7 @@
 
   if (flatpak_dir_use_system_helper (self, NULL))
     {
+#ifndef FLATPAK_DISABLE_GPG
       gboolean gpg_verify_summary;
       gboolean gpg_verify;
 
@@ -15266,6 +15304,7 @@
           g_debug ("Can't update remote configuration as user, no GPG signature");
           return TRUE;
         }
+#endif
 
       if (!flatpak_dir_update_remote_configuration_for_state (self, state, TRUE, &has_changed, cancellable, error))
         return FALSE;
