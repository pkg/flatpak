From af5f4f25f0c31af8a0b4a711d8cbbfca1079c71e Mon Sep 17 00:00:00 2001
From: Arnaud Ferraris <arnaud.ferraris@collabora.com>
Date: Wed, 10 Mar 2021 17:15:25 +0100
Subject: [PATCH 12/26] common: utils: add function for parsing ED25519 public
 keys

This commit lays the groundwork for adding or modifying ED25519-signed
remotes by introducing the flatpak_verify_add_config_options() function.

This function parses a string in the form `KEYTYPE=[inline|file]:DATA`
and adds the corresponding options to the remote config file. These
strings are parsed as is altready implemented in ostree:
  * `KEYTYPE` is the signature algorithm (should be `ed25519` for now)
  * `DATA` is either the base64-encoded public key string for `inline`
    keys, or the name of a file containing base64-encoded public keys
    (1 key per line) for the `file` option
---
 common/flatpak-utils-private.h |  5 +++
 common/flatpak-utils.c         | 57 ++++++++++++++++++++++++++++++++++
 2 files changed, 62 insertions(+)

--- a/common/flatpak-utils-private.h
+++ b/common/flatpak-utils-private.h
@@ -918,6 +918,11 @@
                                    GCancellable  *cancellable,
                                    GError       **error);
 
+char *flatpak_verify_add_config_options (GKeyFile   *config,
+                                         const char *group,
+                                         const char *keyspec,
+                                         GError    **error);
+
 static inline void
 null_safe_g_ptr_array_unref (gpointer data)
 {
--- a/common/flatpak-utils.c
+++ b/common/flatpak-utils.c
@@ -8790,6 +8790,63 @@
   return TRUE;
 }
 
+char *
+flatpak_verify_add_config_options (GKeyFile   *config,
+                                   const char *group,
+                                   const char *keyspec,
+                                   GError    **error)
+{
+  g_autoptr (OstreeSign) sign = NULL;
+  g_auto (GStrv) parts = NULL;
+  g_auto (GStrv) keyparts = NULL;
+  g_autofree char *optname = NULL;
+  const gchar *keytype;
+  const gchar *keyref;
+  const gchar *rest;
+
+  parts = g_strsplit (keyspec, "=", 2);
+  g_assert (parts && *parts);
+  keytype = parts[0];
+  if (!parts[1])
+    {
+      flatpak_fail (error, _("Failed to parse KEYTYPE=[inline|file]:DATA in %s\n"), keyspec);
+      return NULL;
+    }
+
+  sign = ostree_sign_get_by_name (keytype, error);
+  if (!sign)
+    return NULL;
+
+  rest = parts[1];
+  g_assert (!parts[2]);
+  keyparts = g_strsplit (rest, ":", 2);
+  g_assert (keyparts && *keyparts);
+  keyref = keyparts[0];
+  g_assert (keyref);
+
+  if (g_str_equal (keyref, "inline"))
+    {
+      optname = g_strdup_printf ("verification-%s-key", keytype);
+    }
+  else if (g_str_equal (keyref, "file"))
+    {
+      optname = g_strdup_printf ("verification-%s-file", keytype);
+      if (!g_file_test (keyparts[1], G_FILE_TEST_EXISTS))
+        {
+          flatpak_fail (error, _("Verification file %s not found\n"), keyparts[1]);
+          return NULL;
+        }
+    }
+  else
+    {
+      flatpak_fail (error, _("Invalid key reference %s, expected inline|file\n"), keyref);
+      return NULL;
+    }
+
+  g_assert (keyparts[1] && !keyparts[2]);
+  g_key_file_set_string (config, group, optname, keyparts[1]);
+  return g_strdup (ostree_sign_get_name (sign));
+}
 
 #if !GLIB_CHECK_VERSION (2, 56, 0)
 /* All this code is backported directly from glib */
